{-# OPTIONS_GHC -Wall #-}
{-# OPTIONS_GHC -fno-warn-missing-methods #-}

{- Calc
 -
 - http://www.seas.upenn.edu/~cis194/spring13/hw/06-laziness.pdf
 -
 - author: Andrei Alexandru
 -}


module Lazy where

-- Exercise 1 --
fib :: Integer -> Integer
fib 0 = 0
fib 1 = 1
fib x = fib (x-1) + fib (x-2)

fibs1 :: [Integer]
fibs1 = map fib [0..]

-- Exercise 2 --
fib' :: Integer -> Integer -> [Integer]
fib' a b = a : fib' b (a+b)

fibs2 :: [Integer]
fibs2 = fib' 0 1

-- Exercise 3 --
data Stream a = Cons a (Stream a)

streamToList :: Stream a -> [a]
streamToList (Cons x xs) = x : streamToList xs

instance Show a => Show (Stream a) where
  show stream = show . take 20 $ streamToList stream

-- Exercise 4 --
streamRepeat :: a -> Stream a
streamRepeat x = (Cons x (streamRepeat x))

streamMap :: (a -> b) -> Stream a -> Stream b
streamMap f (Cons x xs) = Cons (f x) (streamMap f xs)

streamFromSeed :: (a -> a) -> a -> Stream a
streamFromSeed f seed = Cons seed (streamFromSeed f (f seed))

-- Exercise 5 --
nats :: Stream Integer
nats = streamFromSeed (+1) 0

interleaveStreams :: Stream a -> Stream a -> Stream a
interleaveStreams (Cons x xs) s = Cons x (interleaveStreams s xs)

ruler' :: Integer -> Stream Integer
ruler' x = interleaveStreams (streamRepeat x) (ruler' (x+1))

ruler :: Stream Integer
ruler = ruler' 0

-- Exercise 6 --

-- Calculate fibonacci via generating functions
-- TODO

-- Calculate fibonacci via matrices
data Matrix = Matrix
    { x1 :: Integer, x2 :: Integer
    , y1 :: Integer, y2 :: Integer }
    deriving Show

instance Num Matrix where
    m1 * m2 =
        Matrix
            { x1 = x1 m1 * x1 m2 + x2 m1 * y1 m2
            , x2 = x1 m1 * x2 m2 + x2 m1 * y2 m2
            , y1 = y1 m1 * x1 m2 + y2 m1 * y1 m2
            , y2 = y1 m1 * x2 m2 + y2 m1 * y2 m2 }

fib4 :: Integer -> Integer
fib4 0 = 0
fib4 x = x2 $ Matrix {x1 = 1, x2 = 1, y1 = 1, y2 = 0} ^ x
